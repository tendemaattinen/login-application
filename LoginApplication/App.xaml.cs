﻿using System.Windows;
using LoginApplication.ViewModel;

namespace LoginApplication
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            var window = new MainWindow();
            var viewModel = new NavigationViewModel();
            window.DataContext = viewModel;
            window.Show();
        }
    }
}
