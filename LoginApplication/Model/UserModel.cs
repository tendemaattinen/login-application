﻿using LoginApplication.Utility;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoginApplication.Model
{
    class UserModel
    {
        public string Username { get; set; }
        public int UserId { get; set; }

        public bool CreateNewUser(string username)
        {
            var successfullyCreated = false;
            Username = username;

            try
            {
                var conn = DatabaseHandling.GetConnectionToDatabase(Properties.Settings.Default.PathToDataBase);
                conn.Open();
                var sql = "SELECT Id FROM AccountsTable WHERE Username = @un";
                var command = new SqlCommand(sql, conn);
                command.Parameters.AddWithValue("@un", username);

                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    UserId = (int)reader["Id"];
                }
                conn.Close();
                successfullyCreated = true;

            }
            catch (Exception ex)
            {
                // TODO
                Console.WriteLine(ex.Message);
            }

            return successfullyCreated;
        }
    }
}
