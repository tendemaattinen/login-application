﻿using System;
using System.Windows;
using System.Windows.Controls;
using LoginApplication.PasswordProperty;

namespace LoginApplication.View
{
    /// <summary>
    /// Interaction logic for LoginView.xaml
    /// </summary>
    public partial class LoginView : UserControl
    {
        public LoginView()
        {
            InitializeComponent();
        }

        private void PasswordBox_PasswordChanged(object sender, RoutedEventArgs e)
        {
            var pwBox = sender as PasswordBox;
            LoginPasswordBoxAttachedProperty.SetEncryptedPassword(pwBox, pwBox.SecurePassword);
        }
    }
}
