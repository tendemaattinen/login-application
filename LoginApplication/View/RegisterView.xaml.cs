﻿using System.Windows;
using System.Windows.Controls;
using LoginApplication.PasswordProperty;

namespace LoginApplication.View
{
    /// <summary>
    /// Interaction logic for RegisterView.xaml
    /// </summary>
    public partial class RegisterView : UserControl
    {
        public RegisterView()
        {
            InitializeComponent();
        }

        private void RegisterPasswordBox_PasswordChanged(object sender, RoutedEventArgs e)
        {
            var registerPasswordBox = sender as PasswordBox;
            RegisterPasswordBoxAttachedProperty.SetEncryptedPassword(registerPasswordBox, registerPasswordBox.SecurePassword);
        }

        private void RegisterCheckPasswordBox_PasswordChanged(object sender, RoutedEventArgs e)
        {
            var registerCheckPasswordBox = sender as PasswordBox;
            RegisterCheckPasswordBoxAttachedProperty.SetEncryptedPassword(registerCheckPasswordBox, registerCheckPasswordBox.SecurePassword);
        }
    }
}
