﻿using System;
using System.Data.SqlClient;

namespace LoginApplication.Utility
{
    public static class RegisterAccount
    {
        public static bool AddNewUserToDatabase(string username, string passwordHash)
        {
            var successfulAdding = false;

            try
            {
                var conn = DatabaseHandling.GetConnectionToDatabase(Properties.Settings.Default.PathToDataBase);
                conn.Open();
                var sql = "INSERT INTO AccountsTable (Username, Password) VALUES (@un, @pw)";
                var command = new SqlCommand(sql, conn);
                command.Parameters.AddWithValue("@un", username);
                command.Parameters.AddWithValue("@pw", passwordHash);
                command.ExecuteNonQuery();
                conn.Close();
                successfulAdding = true;
            }
            catch (Exception ex)
            {
                
                Console.WriteLine(ex.Message);
            }
            return successfulAdding;
        }

        public static bool CheckUsernameValidity(string name)
        {
            var isValid = false;

            if (name.Length < 5)
            {
                // TODO
            }
            else if (name.Length >= 50)
            {
                // TODO
            }
            else
            {
                isValid = true;
            }
            return isValid;
        }

        public static bool CheckIfUsernameExistsInDatabase(string name)
        {
            var existsInDatabase = true;
            try
            {
                var conn = DatabaseHandling.GetConnectionToDatabase(Properties.Settings.Default.PathToDataBase);
                conn.Open();
                var sql = "SELECT Id FROM AccountsTable WHERE Username = @un";
                var command = new SqlCommand(sql, conn);
                command.Parameters.AddWithValue("@un", name);

                SqlDataReader reader = command.ExecuteReader();

                if (!reader.HasRows)
                {
                    existsInDatabase = false;
                }

                conn.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return existsInDatabase;
        }
    }
}
