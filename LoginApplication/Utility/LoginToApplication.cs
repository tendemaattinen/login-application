﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Security;

namespace LoginApplication.Utility
{
    public static class LoginToApplication
    {
        public static bool Login(string username, SecureString password)
        {
            var successfulLogin = false;
            string savedPassword = null;
            int userId;
            try
            {
                var conn = DatabaseHandling.GetConnectionToDatabase(Properties.Settings.Default.PathToDataBase);
                conn.Open();
                var sql = "SELECT Id, Password FROM AccountsTable WHERE Username = @un";
                var command = new SqlCommand(sql, conn);
                command.Parameters.AddWithValue("@un", username);

                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    savedPassword = (string)reader["Password"];
                    userId = (int)reader["Id"];
                }

                conn.Close();

                if (!string.IsNullOrEmpty(savedPassword))
                {
                    if (PasswordHandling.CheckIfPasswordsMatch(savedPassword, SecurePasswordHandling.ConvertSecureStringToUnsecure(password)))
                    {
                        successfulLogin = true;
                    }
                }

                else
                {
                    // TODO
                    // Error message.
                }
            }
            catch (Exception ex)
            {
                // TODO
                Console.WriteLine(ex.Message);
            }

            return successfulLogin;
        }
    }
}
