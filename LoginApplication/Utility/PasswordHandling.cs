﻿using System;
using System.Diagnostics;
using System.Security;
using System.Security.Cryptography;

namespace LoginApplication.Utility
{
    public static class PasswordHandling
    {
        public static string GenerateHash(string password)
        {
            // Create salt.
            byte[] salt;
            new RNGCryptoServiceProvider().GetBytes(salt = new byte[16]);

            // New pbkdf2.
            var pbkdf2 = new Rfc2898DeriveBytes(password, salt, 10000);

            byte[] hash = pbkdf2.GetBytes(20);
            byte[] hashBytes = new byte[36];

            Array.Copy(salt, 0, hashBytes, 0, 16);
            Array.Copy(hash, 0, hashBytes, 16, 20);

            string passwordHash = Convert.ToBase64String(hashBytes);

            return passwordHash;
        }

        public static bool CheckIfPasswordsMatch(string savedHash, string password)
        {
            var match = true;

            // Convert password string to bytes.
            byte[] hashBytes = Convert.FromBase64String(savedHash);

            byte[] salt = new byte[16];
            Array.Copy(hashBytes, 0, salt, 0, 16);
            var pbkdf2 = new Rfc2898DeriveBytes(password, salt, 10000);
            byte[] hash = pbkdf2.GetBytes(20);

            // Check if match byte by byte
            for (int i = 0; i < 20; i++)
            {
                if (hashBytes[i + 16] != hash[i])
                {
                    match = false;
                }
            }

            return match;
        }
        
        public static bool CheckPasswordValidity(SecureString securePassword)
        {
            var isValid = false;
            if (SecurePasswordHandling.ConvertSecureStringToUnsecure(securePassword).Length < 5)
            {
                // TODO
            }
            else if (SecurePasswordHandling.ConvertSecureStringToUnsecure(securePassword).Length >= 50)
            {
                // TODO
            }
            else
            {
                isValid = true;
            }

            return isValid;
        }
    }
}
