﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace LoginApplication.Utility
{
    public static class DatabaseHandling
    {
        public static SqlConnection GetConnectionToDatabase(string source)
        {
            SqlConnection conn = null;
            try
            {
                conn = new SqlConnection(source);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return conn;
        }
    }
}
