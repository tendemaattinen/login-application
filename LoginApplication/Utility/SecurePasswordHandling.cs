﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security;
using System.Text;
using System.Threading.Tasks;

namespace LoginApplication.Utility
{
    public static class SecurePasswordHandling
    {
        public static string ConvertSecureStringToUnsecure(SecureString securePassword)
        {
            if (securePassword == null)
            {
                return String.Empty;
            }
            IntPtr unsecurePassword = IntPtr.Zero;

            try
            {
                unsecurePassword = Marshal.SecureStringToGlobalAllocUnicode(securePassword);
                return Marshal.PtrToStringUni(unsecurePassword);
            }
            finally
            {
                Marshal.ZeroFreeGlobalAllocUnicode(unsecurePassword);
            }
        }
    }
}
