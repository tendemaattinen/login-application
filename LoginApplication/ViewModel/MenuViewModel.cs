﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LoginApplication.Model;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;

namespace LoginApplication.ViewModel
{
    class MenuViewModel : ViewModelBase
    {
        private NavigationViewModel _navigationViewModel;

        private UserModel _user;

        public MenuViewModel(NavigationViewModel navigationViewModel, string username)
        {
            _navigationViewModel = navigationViewModel;
            FetchUserInformationFromDatabase(username);
        }

        private ICommand _logout;
        public ICommand Logout
        {
            get
            {
                if (_logout == null)
                {
                    _logout = new RelayCommand(LogoutFromApplication);
                }
                return _logout;
            }
        }

        private ICommand _editProfile;
        public ICommand EditProfile
        {
            get
            {
                if (_editProfile == null)
                {
                    _editProfile = new RelayCommand(EditUsersProfile);
                }
                return _editProfile;
            }
        }


        private void LogoutFromApplication()
        {
            _user = null;
            _navigationViewModel.CurrentView = new LoginViewModel(_navigationViewModel);
        }

        private void EditUsersProfile()
        {
            Console.WriteLine("Here i navigate you to new page!");
        }

        private void FetchUserInformationFromDatabase(string name)
        {
            _user = new UserModel();
            if (_user.CreateNewUser(name))
            {
                Console.WriteLine($"Username: {_user.Username}");
                Console.WriteLine($"User ID: {_user.UserId}");
            }
            else
            {
                // TODO
                // Kick out user and give error message about database error.
            }
        }
    }
}
