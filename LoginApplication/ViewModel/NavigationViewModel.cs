﻿using GalaSoft.MvvmLight;

namespace LoginApplication.ViewModel
{
    class NavigationViewModel : ViewModelBase
    {
        private ViewModelBase _currentViewModel;

        public NavigationViewModel()
        {
            _currentViewModel = new LoginViewModel(this);
            RaisePropertyChanged("CurrentView");
        }

        public ViewModelBase CurrentView
        {
            get
            {
                return _currentViewModel;
            }
            set
            {
                _currentViewModel = value;
                RaisePropertyChanged("CurrentView");
            }
        }

    }
}
