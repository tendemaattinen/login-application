﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using LoginApplication.Utility;
using LoginApplication.View;

namespace LoginApplication.ViewModel
{
    class LoginViewModel : ViewModelBase
    {
        private NavigationViewModel _navigationViewModel;

        public LoginViewModel(NavigationViewModel navigationViewModel)
        {
            _navigationViewModel = navigationViewModel;
        }


        private string _username;
        public string Username
        {
            get
            {
                return _username;
            }
            set
            {
                if (value != null)
                {
                    IsButtonEnabled = true;
                    _username = value;
                }
            }
        }

        private SecureString _securePassword;
        public SecureString SecurePassword
        {
            get { return _securePassword; }
            set
            {
                if (value != null)
                {
                    _securePassword = value;
                }
            }
        }

        private bool _isButtonEnabled = false;
        public bool IsButtonEnabled
        {
            get
            {
                return _isButtonEnabled;
            }
            set
            {
                _isButtonEnabled = value;
                RaisePropertyChanged("IsButtonEnabled");
            }
        }

        private string _isErrorTextVisible = "Hidden";
        public string IsErrorTextVisible
        {
            get
            {
                return _isErrorTextVisible;
            }
            set
            {
                _isErrorTextVisible = value;
                RaisePropertyChanged("IsErrorTextVisible");
            }
        }

        private ICommand _goRegisterView;
        public ICommand GoRegisterView
        {
            get
            {
                if (_goRegisterView == null)
                {
                    _goRegisterView = new RelayCommand(OpenRegisterView);
                }
                return _goRegisterView;
            }
        }

        private void OpenRegisterView()
        {
            _navigationViewModel.CurrentView = new RegisterViewModel(_navigationViewModel);
        }

        private ICommand _login;
        public ICommand Login
        {
            get
            {
                if (_login == null)
                {
                    _login = new RelayCommand(LoginMethod, CanLoginApplication);
                }
                return _login;
            }
        }

        private void LoginMethod()
        {
            var check = LoginToApplication.Login(Username, SecurePassword);
            if (check)
            {
                _navigationViewModel.CurrentView = new MenuViewModel(_navigationViewModel, Username);
            }
            else
            {
                IsErrorTextVisible = "Visible";
            }
        }

        private bool CanLoginApplication()
        {
            return true;
        }
    }
}
