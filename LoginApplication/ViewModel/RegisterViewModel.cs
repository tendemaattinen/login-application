﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using LoginApplication.Utility;
using System.Windows.Media;
using System.Windows;

namespace LoginApplication.ViewModel
{
    class RegisterViewModel : ViewModelBase
    {
        NavigationViewModel _navigationViewModel;

        public RegisterViewModel(NavigationViewModel navigationViewModel)
        {
            _navigationViewModel = navigationViewModel;
        }

        public string Username { get; set; }

        private SecureString _userPassword;
        public SecureString UserPassword
        {
            get
            {
                return _userPassword;
            }
            set
            {
                if (value != null)
                {
                    _userPassword = value;
                }
            }
        }

        private SecureString _userPassword2;
        public SecureString UserPassword2
        {
            get
            {
                return _userPassword2;
            }
            set
            {
                if (value != null)
                {
                    _userPassword2 = value;
                }
            }
        }

        private Color _colorUsername = (Color)Application.Current.Resources["TextColor"];
        public Color ColorUsername
        {
            get
            {
                return _colorUsername;
            }
            set
            {
                if (value != null)
                {
                    _colorUsername = value;
                    RaisePropertyChanged("ColorUsername");
                }
            }
        }

        private string _colorPassword = "Black";
        public string ColorPassword
        {
            get
            {
                return _colorPassword;
            }
            set
            {
                if (value != null)
                {
                    _colorPassword = value;
                    RaisePropertyChanged("ColorPassword");
                }
            }
        }

        private string _errorText;
        public string ErrorText
        {
            get
            {
                return _errorText;
            }
            set
            {
                if (value != null)
                {
                    _errorText = value;
                    RaisePropertyChanged("ErrorText");
                }
            }
        }

        private string _errorTextVisibility = "Hidden";
        public string ErrorTextVisibility
        {
            get
            {
                return _errorTextVisibility;
            }
            set
            {
                if (value != null)
                {
                    _errorTextVisibility = value;
                    RaisePropertyChanged("ErrorTextVisibility");
                }
            }
        }

        private string _errorTextColor = "Red";
        public string ErrorTextColor
        {
            get
            {
                return _errorTextColor;
            }
            set
            {
                if (value != null)
                {
                    _errorTextColor = value;
                    RaisePropertyChanged("ErrorTextColor");
                }
            }
        }

        private ICommand _goToLoginPage;
        public ICommand GoToLoginPage
        {
            get
            {
                if (_goToLoginPage == null)
                {
                    _goToLoginPage = new RelayCommand(GoLoginPage);
                }
                return _goToLoginPage;
            }
        }

        private void GoLoginPage()
        {
            _navigationViewModel.CurrentView = new LoginViewModel(_navigationViewModel);
        }

        private ICommand _register;
        public ICommand Register
        {
            get
            {
                if (_register == null)
                {
                    _register = new RelayCommand(RegisterNewAccount);
                }
                return _register;
            }
        }

        private void RegisterNewAccount()
        {
            ColorUsername = (Color)Application.Current.Resources["TextColor"];
            ColorPassword = "Black";
            ErrorTextColor = "Black";
            ErrorTextVisibility = "Visible";
            if (SecurePasswordHandling.ConvertSecureStringToUnsecure(UserPassword) != SecurePasswordHandling.ConvertSecureStringToUnsecure(UserPassword2))
            {
                ColorPassword = "Red";
                ErrorText = "Passwords won't match!";
            }
            else if (!PasswordHandling.CheckPasswordValidity(UserPassword))
            {
                ColorPassword = "Red";
                ErrorText = "Password is not valid!";
            }
            else if (!RegisterAccount.CheckUsernameValidity(Username))
            {
                ColorUsername = (Color)Application.Current.Resources["ErrorTextColor"];
                ErrorText = "Username is not valid!";
            }
            else if (RegisterAccount.CheckIfUsernameExistsInDatabase(Username))
            {
                ColorUsername = (Color)Application.Current.Resources["ErrorTextColor"];
                ErrorText = "Username already exists!";
            }
            else
            {
                if (RegisterAccount.AddNewUserToDatabase(Username, PasswordHandling.GenerateHash(SecurePasswordHandling.ConvertSecureStringToUnsecure(UserPassword))))
                {
                    Username = "";
                    UserPassword = new SecureString();
                    UserPassword2 = new SecureString();
                    ErrorText = "Registration was successful!";
                }
                else
                {
                    ErrorTextColor = "Red";
                    ErrorText = "Registration failed!";
                }
            }
        }
    }
}
