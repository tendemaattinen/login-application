﻿using LoginApplication.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace LoginApplicationTests
{
    public class RegisterTests
    {
        [Theory]
        [InlineData ("PekkaJaa")]
        [InlineData("Keijo")]
        [InlineData("1234567890")]
        public void CheckUsernameValidity_ValidNames_ReturnsTrue(string name)
        {
            Assert.True(RegisterAccount.CheckUsernameValidity(name));
        }

        [Theory]
        [InlineData ("6")]
        [InlineData ("Jos")]
        [InlineData("123")]
        public void CheckUsernameValidity_TooShortNames_ReturnsFalse(string name)
        {
            Assert.False(RegisterAccount.CheckUsernameValidity(name));
        }

        [Theory]
        [InlineData("43225b53n637n8m65m88m756m875m8m58m58m5856m85m4m57m465m56mm865m868m56856m8m568m5688658m56m856m856m58m568568m568m5m85m856m88m755m6765m")]
        [InlineData("Josn7nee7nyyrcsnjvntysnuivynuvsiynuvtiuylvtlkintaliytavytvaytanvrliytanuvirtvnyauirvtaiyurnltyvaiurnltyauvringvdbkhtkuhbbydtrybt")]
        [InlineData("123346879893484367893468364328766893678367384967839768739467890346873468973786435678689347683768347634896783679834667878567896876876")]
        public void CheckUsernameValidity_TooLongNames_ReturnsFalse(string name)
        {
            Assert.False(RegisterAccount.CheckUsernameValidity(name));
        }
    }
}
